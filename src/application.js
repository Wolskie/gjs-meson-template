/* window.js
 *
 * Copyright 2018 Mark Hahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const {
    Gdk,
    Gio,
    GLib,
    GObject,
    Gtk
} = imports.gi;

const MainWindow = imports.mainWindow;

var Application = GObject.registerClass({}, class Application extends Gtk.Application {
    _init() {
        super._init({
            application_id: 'org.gnome.Avalon',
            flags: Gio.ApplicationFlags.HANDLES_OPEN
        });

        GLib.set_prgname('avalon');
    }

    vfunc_activate() {
        let window = new MainWindow.MainWindow({
            application: this
        });
        window.show();
    }
});